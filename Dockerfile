ARG DEV=0

### BASE ###

FROM python:3.9-slim as base

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONHASHSEED=random \
    PYTHONUNBUFFERED=1 \
    DEBIAN_FRONTEND=noninteractive

RUN apt update && apt install -y default-jdk && rm -rf /var/lib/apt/lists/*

### BUILDER ###

FROM base as builder
ARG DEV

ENV PIP_DEFAULT_TIMEOUT=100 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1 \
    POETRY_VERSION=1.1.14

RUN apt update && apt install -y curl

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="/root/.local/bin/:$PATH"

RUN python -m venv /venv

COPY pyproject.toml poetry.lock ./
RUN poetry export -f requirements.txt --without-hashes $(test $DEV -eq 1 && echo "--dev") \
    | /venv/bin/pip install -r /dev/stdin # --no-deps


COPY . .
RUN poetry build && /venv/bin/pip install dist/*.whl

### RUNTIME ###

FROM base as runtime

COPY --from=builder /venv /venv

ENV PATH="/venv/bin/:$PATH" \
    VIRTUAL_ENV="/venv"

ENV PROJECT_ROOT="/app"
WORKDIR $PROJECT_ROOT

COPY src .


### SCRAPER ###

FROM runtime as scraper
WORKDIR /app/publicidade_recife
