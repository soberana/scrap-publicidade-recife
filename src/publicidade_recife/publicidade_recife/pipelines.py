# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import os

import pandas as pd
import tabula
from scrapy.exceptions import DropItem

from publicidade_recife.items import PublicidadeDataFrameItem

""

# from publicidade_recife.settings import (BUCKET_URI, WASABI_ACCESS_KEY_ID,
#                                          WASABI_SECRET_ACCESS_KEY)


class PublicidadeRecifePipeline:
    def process_item(self, item, spider):

        try:
            pages = tabula.read_pdf(item["pdf_link"], pages="all")
            df = pd.concat(pages)
        except:
            raise DropItem(
                f'Não há tabela em: {item["org"]}\t{item["year"]}\t{item["month"]}\t{item["pdf_link"]}'
            )

        df["year"] = item["year"]
        df["month"] = extract_month(item["month"])
        df["org"] = item["org"]

        outdir = f"data/{item['org']}/{item['year']}"
        if not os.path.exists(outdir):
            os.makedirs(outdir, exist_ok=True)

        try:
            df.columns = [
                "agencia",
                "descricao_campanha",
                "subcontratada",
                "valor_total",
                "honor",
                "vl_subc",
                "empenho",
                "nf_ag",
                "nf_forn",
                "contrato",
                "classificacao",
                "subelemento",
                "year",
                "month",
                "org",
            ]
        except:
            raise DropItem(
                f'A tabela não segue o padrão esperado: {item["org"]}\t{item["year"]}\t{item["month"]}\t{item["pdf_link"]}'
            )

        # TODO: Teste!
        valores = ["valor_total", "honor", "vl_subc"]
        for v in valores:
            df[v] = (
                df[v]
                .str.replace("R$", "")
                .str.replace("-", "")
                .str.replace(".", "")
                .str.replace(",", ".")
                .str.replace(" ", "")
                .astype("float")
            )

        df["ano_empenho"], df["empenho"], df["subempenho"] = unpack_empenho(
            df["empenho"]
        )

        relevante_cols = [
            "org",
            "agencia",
            "descricao_campanha",
            "subcontratada",
            "contrato",
            "valor_total",
            "honor",
            "vl_subc",
            "empenho",
            "ano_empenho",
            "subempenho",
            "subelemento",
        ]

        df = df[relevante_cols]

        df = df.drop_duplicates()

        # df.to_csv(f"{outdir}/{item['month']}.csv", index=False)
        return PublicidadeDataFrameItem(**df.to_dict(orient="records"))
        # return df.to_dict(orient="list")
        # return df.to_json(orient="split")


def extract_month(anchor_str):
    """
    Extrai o mês da âncoira do link.

    Na página [0] a âncora do link contendo o pdf está no formato:
    [MÊS] de [ANO]
    Esta fuunção retira a primeira palavra e verifica se está é um mês válido

    [0] http://transparencia.recife.pe.gosv.br/codigos/web/estaticos/estaticos.php?nat=DDG&filhoNatureza=1249#filho
    """
    months = [
        "jan",
        "fev",
        "mar",
        "abr",
        "mai",
        "jun",
        "jul",
        "ago",
        "set",
        "out",
        "nov",
        "dez",
    ]

    month_str = anchor_str.split(" ")[0]
    month = month_str[:3].lower()
    if month in months:
        return month
    else:
        raise DropItem(f"{month_str} ({month}) não parece ser um mês válido!")


def unpack_empenho(empenho: pd.Series) -> (int, int, int):
    _ano_empenho, _subempenho = empenho.str.split("-").str
    _ano, _empenho = _ano_empenho.str.split(".").str
    return _ano.astype(int), _empenho.astype(int), _subempenho.astype(int)
