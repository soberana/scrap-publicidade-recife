# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from typing import List

from scrapy import Field, Item


class PublicidadeRecifeItem(Item):
    # define the fields for your item here like:
    org = Field()
    year = Field()
    month = Field()
    pdf_link = Field()


class PublicidadeDataFrameItem(Item):
    org: List[str] = Field()
    agencia: List[str] = Field()
    descricao_campanha: List[str] = Field()
    subcontratada: List[str] = Field()
    contrato: List[str] = Field()
    valor_total: List[float] = Field()
    honor: List[float] = Field()
    vl_subc: List[float] = Field()
    empenho: List[int] = Field()
    ano_empenho: List[int] = Field()
    subempenho: List[int] = Field()
    subelemento: List[int] = Field()
