#!/usr/bin/env ipython
from scrapy import Spider

from publicidade_recife.items import PublicidadeRecifeItem
from loguru import logger

class PublicidadeSpider(Spider):
    name = "publicidade_recife"
    start_urls = [
        "http://transparencia.recife.pe.gov.br/codigos/web/estaticos/estaticos.php?nat=DDG&filhoNatureza=1249#filho"
    ]

    def parse(self, response):
        """Publicidade's parse in Portal da Transparência.

        Parse Portal da Transparência to retrieve pdfs' links with
        Publicidade's data by month/year
        """
        organizations = response.xpath(
            "//nav/ul/li/span[contains(text(), 'Despesas com Publicidade')]/../ul/li"
        )
        _dict = {}
        for _org in organizations:
            org = _org.xpath("./span/text()").get()
            _dict[org] = {}
            years = _org.xpath("./ul/li")
            for _year in years:
                year = _year.xpath("./span/text()").get()
                _dict[org][year] = {}
                months = _year.xpath("./ul/li")
                for _month in months:
                    month = _month.xpath("./a/text()").get()
                    pdf_link = _month.xpath("./a/@href").get()
                    _dict[org][year][month] = pdf_link
                    yield PublicidadeRecifeItem(
                        org=org, year=year, month=month, pdf_link=pdf_link
                    )
