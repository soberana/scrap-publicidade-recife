#!/usr/bin/env python3

import pandas as pd


def get_planilha(xlsx_link):
    df = pd.read_excel(xlsx_link, header=1)
    return df
