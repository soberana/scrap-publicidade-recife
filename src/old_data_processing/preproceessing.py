#!/usr/bin/env ipython

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


import io
import logging
# useful for handling different item types with a single interface
import os

# import camelot
import pandas as pd
import tabula
from requests.utils import requote_uri
from scrapy.exceptions import DropItem

from data_wrangling_publi_recife.xlxs_parser import get_planilha

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("Preprocessing")

buffer1 = io.StringIO()

def debug_describe_df(df: pd.DataFrame, status="debug"):
    df.info(buf=buffer1)
    s = f"\t Dataframe's info:\n{buffer1.getvalue()}\n"
    if status == "debug":
        logger.debug(s)
    else:
        logger.warn(s)


def clean_camelot_result(df):
    first_line = df.iloc[0]
    df.columns = first_line
    df = df.iloc[1:].reset_index(drop=True)
    return df


def concat_camelot_results(pages):
    df_list = [clean_camelot_result(page.df) for page in pages]
    df = pd.concat(df_list)
    return df


def encode_url(url):
    return requote_uri(url)


def table_validation(df):
    pass


def generate_org_dir(org, year):
    outdir = f"data/{org}/{year}"
    if not os.path.exists(outdir):
        os.makedirs(outdir, exist_ok=True)


def debug_rules(org, year, month_str, pdf_link, parser):
    rules = []
    rules.append(month_str.__contains__("Janeiro"))

    if all(rules):
        return True
    else:
        logger.warn("Do not pass debug rules:")
        logger.info(f"{org}\t{year}\t{month_str}\t{pdf_link}\t{parser}\t")
        return False


def fix_anomalies(df):
    print("######### FIX ANOMALIES #########")
    if any(df[df["valor_total"].str.contains("103,666,40")]):
        # valor_total = 103,666,40
        print(df[df["valor_total"].str.contains("103,666,40")])
        print("# Valor total com duas vírgulas")
        print("OLD: 103,666,40")
        print("NEW: 103666,40")
        df["valor_total"] = df["valor_total"].str.replace("103,666,40", "103666,40")
    if any(df["empenho"].str.contains("2023.00138-09 E 2023.00191-16")):
        # empenho = 2023.00138-09 E 2023.00191-16
        print("# Dois empenhos em uma célula")
        print("OLD: 2023.00138-09 E 2023.00191-16")
        print("NEW: 2023.00138-09")
        df["empenho"] = df["empenho"].str.replace(
            "2023.00138-09 E 2023.00191-16", "2023.00138-09"
        )
    print("######### END FIX ANOMALIES #########")
    return df

def adjust_columns(df, col_len=12):
    if len(df.columns) > col_len:
        logger.warn(">>>")
        logger.warn(df.columns[col_len:])
        df = df[df.columns[:col_len]]
        logger.warn(">>>")
        logger.warn(df.iloc[0])
    return df

def process_item(org, year, month_str, pdf_link, MODE, parser="tabula"):

    # if not debug_rules(org, year, month_str, pdf_link, parser):
    #     return pd.DataFrame()
    pdf_file = pdf_link.split("/")[-1]
    logger.info(f"\tDownloading table in:\t{org}\t{year}\t{month_str}\t\t{pdf_file}")
    generate_org_dir(org, year)
    COLS = [
        ['AGÊNCIA', 'DESCRIÇÃO / CAMPANHA', 'SUBCONTRATADA', 'VALOR TOTAL',
       'HONORÁRIOS', 'VALOR\rSUBCONTRATADA', 'NÚMERO\rEMPENHO', 'NF AGÊNCIA',
       'NF\rFORNECEDO\rR', 'CONTRATO', 'CLASSIFICAÇÃO', 'SUBELEMENTO']
    ]
    logger.debug(f"\tMode {MODE}")
    if MODE == "pdf":
        try:
            logger.debug(f"\tParser {parser}")
            if parser == "camelot":
                raise "Camelot are not usable in Docker (need some extra libs to work)"
                # pages = camelot.read_pdf(encode_url(pdf_link), pages="all")
                # df = concat_camelot_results(pages)
            if parser == "tabula":
                # TODO: Multi table validation
                pdf_multi_tables = True if (year < 2023) else False
                pdf_multi_tables = True
                pages = tabula.read_pdf(
                    pdf_link, pages="all",
                    # pandas_options={"usecols": COLS},
                    multiple_tables=pdf_multi_tables
                    )
                df = pd.concat(pages)
                # TODO: df.info() does not work =/
        except Exception as e:
            logger.warn(e)
            logger.warn(f"\tThere are no tables in:\t{org}\t{year}\t{month_str}\t\t{pdf_file}")
            return None
    if MODE == "xlsx":
        df = get_planilha(encode_url(pdf_link))

    debug_describe_df(df)
    df = adjust_columns(df)
    # debug_describe_df(df)
    # df = df.dropna()
    # logger.info(f">>>>TAMANHO DF>>>>>> {len(df)}")

    df["year"] = year
    df["month"] = extract_month(month_str)
    df["org"] = org
    debug_describe_df(df)

    try:
        df.columns = [
            "agencia",
            "descricao_campanha",
            "subcontratada",
            "valor_total",
            "honor",
            "vl_subc",
            "empenho",
            "nf_ag",
            "nf_forn",
            "contrato",
            "classificacao",
            "subelemento",
            "year",
            "month",
            "org",
        ]
        # df = df.dropna(subset=["valor_total", "empenho"])
    except:
        logger.warn(
            f"\tThe Dataframe is not in a waited pattern: {org}\t{year}\t{month_str}\t{pdf_link}"
        )
        logger.warn(f"\tDataframe columns:\n\t{df.columns}")
        return None

    debug_describe_df(df)
    df = fix_anomalies(df)
    debug_describe_df(df)

    # TODO: Teste!
    if MODE == "pdf":
        valores = ["valor_total", "honor", "vl_subc"]
        for v in valores:
            df[v] = df[v].astype(str)
            df[v] = (
                df[v]
                .str.replace("[A-Za-z]", "", regex=True)
                .str.replace("$", "", regex=False)
                .str.replace("-", "", regex=False)
                .str.replace(".", "", regex=False)
                .str.replace(",", ".", regex=False)
                .str.replace(" ", "", regex=False)
                .str.replace("(", "", regex=False)
                .str.replace(")", "", regex=False)
            )
            df[v].replace("", 0)
    debug_describe_df(df)

    table_validation(df)

    df.to_csv("data/test_df.csv", index=False, sep=";")
    # pd.read_csv("data/test_df.csv")
    input()
    df["ano_empenho"], df["empenho"], df["subempenho"] = unpack_empenho(df["empenho"], null_pattern="0NE0")
    logger.info(f">>>>TAMANHO DF>>>>>> {len(df)}")
    df = df.fillna(value=0)
    df = df.replace("", 0)
    logger.info(f">>>>TAMANHO DF>>>>>> {len(df)}")
    relevante_cols = [
        "org",
        "agencia",
        "descricao_campanha",
        "subcontratada",
        "contrato",
        "valor_total",
        "honor",
        "vl_subc",
        "empenho",
        "ano_empenho",
        "subempenho",
        "subelemento",
        "year",
        "month",
    ]

    df = df[relevante_cols]

    df = df.drop_duplicates()
    logger.info(f">>>>TAMANHO DF>>>>>> {len(df)}")

    df = capitalize_str(df)
    logger.info(f">>>>>>soma do mes>>>>>>>>>>> {df.valor_total.astype(float).sum()}")
    df = df[df["vl_subc"].astype(float) < 100000000]
    df.to_csv(f"data/{org}/{year}/df.csv", index=False)

    return df


def capitalize_str(df):
    for col in retrieve_str_cols(df):
        df[col] = df[col].str.upper()
    return df


def retrieve_str_cols(df):
    dt = df.dtypes
    str_cols = dt[dt == "object"].index
    return str_cols


def extract_month(anchor_str):
    """
    Extrai o mês da âncoira do link.

    Na página [0] a âncora do link contendo o pdf está no formato:
    [MÊS] de [ANO]
    Esta fuunção retira a primeira palavra e verifica se está é um mês válido

    [0] http://transparencia.recife.pe.gosv.br/codigos/web/estaticos/estaticos.php?nat=DDG&filhoNatureza=1249#filho
    """
    months = [
        "jan",
        "fev",
        "mar",
        "abr",
        "mai",
        "jun",
        "jul",
        "ago",
        "set",
        "out",
        "nov",
        "dez",
    ]

    month_str = anchor_str.split(" ")[0]
    month = month_str[:3].lower()
    if month in months:
        return month
    else:
        raise DropItem(f"{month_str} ({month}) não parece ser um mês válido!")
        return None


def unpack_empenho(empenho: pd.Series, null_pattern: str = "0.0-0") -> (int, int, int):
    # 2023.00138-09 E 2023.00191-16
    try:
        empenho = empenho.fillna(null_pattern)
        if null_pattern == "0.0-0":
            _df = empenho.str.split("-", n=1, expand=True)
            _df = _df.fillna(value=0)
            _ano_empenho, _subempenho = _df[0], _df[1]
            _df = _ano_empenho.str.split(".", n=1, expand=True)
            _df = _df.fillna(value=0)
            _ano, _empenho = _df[0], _df[1]
        elif null_pattern == "0NE0":
            _df = empenho.str.split("NE", n=1, expand=True)
            _df[2] = 0
            _df = _df.fillna(value=0)
            _ano, _empenho, _subempenho = _df[0], _df[1], _df[2]
    except:
        logger.error(empenho)
        logger.error("---")
        raise
    try:
        _ano, _empenho, _subempenho = (
            _ano.astype(int),
            _empenho.astype(int),
            _subempenho.astype(int),
        )
    except ValueError as e:
        # for emp in empenho:
        #     logger.warn(emp)
        logger.warn(_ano.unique())
        logger.warn(_empenho.unique())
        logger.warn(_subempenho.unique())
        raise e
    except TypeError as e:
        for emp in empenho:
            logger.warn(emp)
        logger.warn(_ano)
        logger.warn(_empenho.unique())
        logger.warn(_subempenho.unique())
        raise e


    return _ano, _empenho, _subempenho
