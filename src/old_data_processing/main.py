#!/usr/bin/env ipython
import logging
import pandas as pd

from data_wrangling_publi_recife.preproceessing import process_item
from data_wrangling_publi_recife.upload_df_to_s3 import upload_to_wasabi

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("Preprocessing")

MODE = "pdf"  # pdf | xlsx

df_scrap = pd.read_json(
    "https://s3.us-east-1.wasabisys.com/scrap-publicidade/portal-da-transparencia/pdf-list.json"
)

df_scrap.month = df_scrap.month.str.replace("\xa0", "")

# df_scrap = df_scrap[df_scrap["year"] < 2023]
df_scrap = df_scrap[df_scrap["year"] >= 2021]
df_scrap = df_scrap[df_scrap["pdf_link"].str.contains(f".{MODE}", regex=False)]
df_scrap = df_scrap[df_scrap["org"].str.contains("Secretaria de Governo - SEGOV", regex=False)]
df_scrap = df_scrap.reset_index(drop=True)


df_scrap["pdf_link"] = [ url if url[:8] == "https://" else "https://transparencia.recife.pe.gov.br" + url for url in df_scrap["pdf_link"] ]


logger.info(f"\nDataFrame head:\n{df_scrap.head(2)}\n...")
# input()

result_list = [
    process_item(org, year, month, pdf, MODE)
    for _, (org, year, month, pdf) in df_scrap.iterrows()
]

df = pd.concat(result_list)
df = df.drop_duplicates()

file_path = "data/data_test_full.csv"
bucket_name = "scrap-publicidade"
key_name = "publicidade_recife/df_app_new.csv"

df.to_csv(file_path, index=False)
upload_to_wasabi(file_path, bucket_name, key_name)
