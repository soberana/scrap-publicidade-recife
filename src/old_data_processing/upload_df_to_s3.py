#!/usr/bin/env python3
import os
from os.path import dirname, join

import boto3
from dotenv import load_dotenv

dotenv_path = join(dirname(__file__), ".env")
load_dotenv(dotenv_path)


ACCESS_KEY_ID = os.environ.get("WASABI_ACCESS_KEY_ID")
SECRET_ACCESS_KEY = os.environ.get("WASABI_SECRET_ACCESS_KEY")
ENDPOINT_URL = "https://s3.us-east-1.wasabisys.com"


def upload_to_wasabi(file_path, bucket_name, key_name):
    """Upload a object to Wasabi S3."""
    s3 = boto3.client(
        "s3",
        endpoint_url="https://s3.wasabisys.com",
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=SECRET_ACCESS_KEY,
    )
    s3.upload_file(file_path, bucket_name, key_name)
