#!/usr/bin/env python3
from pydantic_settings import BaseSettings
from pathlib import Path


class Settings(BaseSettings):
    source_extension: str = "pdf"
    source_url: str = "https://s3.us-east-1.wasabisys.com/scrap-publicidade/portal-da-transparencia/pdf-list.json"
    source_tables_url_base: str = "https://transparencia.recife.pe.gov.br"
    source_columns: list[str] = [
        "AGÊNCIA",
        "DESCRIÇÃO / CAMPANHA",
        "SUBCONTRATADA",
        "VALOR TOTAL",
        "HONORÁRIOS",
        "VALOR\rSUBCONTRATADA",
        "NÚMERO\rEMPENHO",
        "NF AGÊNCIA",
        "NF\rFORNECEDO\rR",
        "CONTRATO",
        "CLASSIFICAÇÃO",
        "SUBELEMENTO",
    ]
    target_columns: list[str] = [
        "agencia",
        "descricao_campanha",
        "subcontratada",
        "valor_total",
        "honor",
        "vl_subc",
        "empenho",
        "nf_ag",
        "nf_forn",
        "contrato",
        "classificacao",
        "subelemento",
        "year",
        "month",
        "org",
    ]
    final_cols: list[str] = [
        "org",
        "agencia",
        "descricao_campanha",
        "subcontratada",
        "contrato",
        "valor_total",
        "honor",
        "vl_subc",
        "empenho",
        "ano_empenho",
        "subempenho",
        "subelemento",
        "year",
        "month",
    ]
    target_values_columns: list[str] = ["valor_total", "honor", "vl_subc"]
    target_initial_year: int = 2021
    target_last_year: int = 2024
    target_org: str = "Secretaria de Governo - SEGOV"
    target_data_dir: Path = Path("../data")
    parser: str = "tabula"

    target_monthly_sum: dict = {
        # 2024
        "jan/2024": 1868940.37,
        "fev/2024": 2468326.05,
        "mar/2024": 5585972.57,
        # 2023
        "jan/2023": 1651683.78,  # R$ 1.651.683,78
        "fev/2023": 3678256.41,  # R$ 3.678.256,41
        "mar/2023": 6170972.68,  # R$ 6.161.743,68 <---
        "abr/2023": 6432821.02,  # R$ 6.432.821,02
        "mai/2023": 5177979.13,  # R$ 2.235.993,28 <--- !!!
        "jun/2023": 8724293.72,  # R$ 8.827.960,12 <---
        "jul/2023": 6498593.49,  # R$ 6.498.593,49
        "ago/2023": 6470527.06,  # R$ 6.470.527,06
        "set/2023": 5269445.46,  # R$ 5.269.445,46
        "out/2023": 6795419.44,  # R$ 7.032.919,44 <---
        "nov/2023": 5830395.79,  # R$ 5.830.395,79
        "dez/2023": 9223188.25,  # R$ 9.223.188,25
    }


settings = Settings()
