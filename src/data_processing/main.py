#!/usr/bin/env python3
#!/usr/bin/env ipython
import sys
import pandas as pd

from data_processing.config import settings
from data_processing.scrap_processing import scrap_cleaning
from data_processing.tables_processing import tables_processing
from data_processing.utils.upload import upload_to_wasabi
from loguru import logger

pd.options.mode.copy_on_write = True

logger.remove()
logger.add(
    sys.stdout, colorize=True, format="<green>{level}</green> <level>{message}</level>"
)
logger.add(
    "log.txt",
    level="WARNING",
    colorize=True,
    format="<green>{level}</green> <level>{message}</level>",
)


logger.info("Application initialization")
logger.info(f"Settings init:\n\t{settings}")

logger.info("Loading source table with dataframe urls")
df_scrap = pd.read_json(settings.source_url)

logger.info("Cleaning scrap file with tables references and metadata")
df_scrap = scrap_cleaning(df_scrap, settings)

logger.info("Processing tables")
df = tables_processing(df_scrap, settings)

logger.info("Showing result DataFrame")
logger.info(df.head())

_parquet_data = f"{settings.target_data_dir}/parquet"
df.to_parquet(_parquet_data)

bucket_name = "scrap-publicidade"
key_name = "publicidade_recife/df_app"
upload_to_wasabi(_parquet_data, bucket_name, key_name)
