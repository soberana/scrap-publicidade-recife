#!/usr/bin/env python3
from loguru import logger
import tabula
import pandas as pd


def tabula_parser(url: str, multi_tables: bool = True):
    try:
        pages = tabula.read_pdf(
            url,
            pages="all",
            # pandas_options={"usecols": COLS},
            multiple_tables=multi_tables,
        )
        df = pd.concat(pages)
    except Exception as e:
        logger.error(e)
    return df
