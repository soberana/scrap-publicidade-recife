# /bin/env python3
from pathlib import Path

import pandas as pd
from loguru import logger

from data_processing.config import Settings
from data_processing.extract import tabula_parser
from data_processing.utils.dates import extract_month_from_anchor
from data_processing.utils.dirs import extract_pdf_file, generate_data_dir
from data_processing.utils.transform_str_cols import (
    split_empenho_patterns,
    adjust_values_fields,
    capitalize_str,
)


def tables_processing(df: pd.DataFrame, config: Settings):
    logger.info("Extracting tables")
    tables_list = [
        pipeline_df(org, year, month, pdf, config)
        for _, (org, year, month, pdf) in df.iterrows()
    ]
    logger.info("Merging tables")
    df = pd.concat(tables_list)
    df = df.drop_duplicates()

    # df = adjust_types(df)

    output_dir = generate_data_dir(config.target_data_dir, "_final")
    store_partial_df(df, output_dir, "final")

    logger.info("")
    df = post_processing(df, config)

    evaluate_by_month(df, config)
    store_partial_df(df, output_dir, "dashboard")

    return df


def evaluate_by_month(df, config):
    years = [2024, 2023]
    months = df["month"].unique()

    for _y in years:
        for _m in months:
            _df = df[(df["month"] == _m) & (df["year"] == _y)]
            evaluate_monthly_sum(_df, f"{_m}/{_y}", config)


def post_processing(df: pd.DataFrame, config: Settings):
    df = df[config.final_cols]
    df = capitalize_str(df)
    return df


def pipeline_df(org: str, year: str, month_str: str, pdf_link: str, config: Settings):
    logger.info(f"### Starting pipeline for DataFrame:\t{org}\t{year}\t{month_str} ###")
    file_extension = config.source_extension
    output_dir = generate_data_dir(config.target_data_dir, extract_pdf_file(pdf_link))
    logger.info(f"Data output directory: {output_dir}")
    n_cols_expected = len(config.source_columns)
    logger.info(f"\tDownloading df from:\n\t\t{pdf_link}")

    if file_extension == "pdf":
        df = download_df_pdf(pdf_link, config.parser, n_cols_expected)
        if df is None:
            return df
    else:
        raise "Just accepting pdf files to parser"

    store_partial_df(df, output_dir, "post_download")

    logger.info("\tAdding metadata as a DataFrame columns")
    month = extract_month_from_anchor(month_str)
    df["year"] = year
    df["month"] = month
    df["org"] = org

    logger.info("\tAdjusting DataFrame column names")
    df = adjust_columns(df, config.target_columns)

    store_partial_df(df, output_dir, "post_columns")

    logger.info("\tAdjusting DataFrame values columns")
    df = adjust_values_fields(df, config.target_values_columns)

    store_partial_df(df, output_dir, "post_values")

    logger.info("\tUnpacking `empenho` columns")
    logger.info("\t\tSpliting `empenho` by patterns")
    df_split_empenho = split_empenho_patterns(df)
    _df_dash_dot = unpack_empenho_dash_dot(df_split_empenho["dash_dot"])
    _df_ne = unpack_empenho_ne(df_split_empenho["ne"])
    df = pd.concat([_df_dash_dot, _df_ne])
    df["empenho"] = df["empenho"].astype("int")

    store_partial_df(df, output_dir, "post_empenho_unpack")

    # TODO: fillna
    # TODO: replace
    # TODO: filter relevant cols
    # TODO: drop duplicates

    evaluate_monthly_sum(df, f"{month}/{year}", config)
    store_partial_df(df, output_dir, "final")
    logger.info("### DONE! ###")
    return df


def evaluate_monthly_sum(df: pd.DataFrame, month_year: str, config: Settings):
    monthly_sum = round(df["valor_total"].sum(), 2)
    logger.warning(f"\tMONTHLY SUM: {month_year} {monthly_sum}")
    try:
        _ms = config.target_monthly_sum[month_year]
        _ms_diff = _ms - monthly_sum
        if _ms_diff != 0:
            logger.warning(
                f"\t{month_year}: Target {_ms} | Scraped {monthly_sum} | Diff {_ms_diff}"
            )
            # input()
    except:
        pass


def store_partial_df(
    df: pd.DataFrame, output_dir: Path, partial_name: str, parquet=False
):
    if df is not None:
        _parquet = f"{output_dir}/{partial_name}"
        _csv = _parquet + ".csv"
        df.to_csv(_csv, index=False, sep=";")
        if parquet:
            df.to_parquet(_parquet)


def download_df_pdf(pdf_link, parser, n_cols_expected=12):
    try:
        if parser == "tabula":
            df = tabula_parser(pdf_link, multi_tables=True)
            n_cols_returned = len(df.columns)
            if n_cols_returned != n_cols_expected:
                logger.warning(
                    f"\tNumber of columns returned/expected: {n_cols_returned}/{n_cols_expected}"
                )
                logger.warning("\tParsing with multiple tables parameter FALSE")
                df = tabula_parser(pdf_link, multi_tables=False)
                n_cols_returned = len(df.columns)
            assert n_cols_returned == n_cols_expected
        elif parser == "camelot":
            raise "Camelot are not usable in Docker (need some extra libs to work)"
            # pages = camelot.read_pdf(encode_url(pdf_link), pages="all")
            # df = concat_camelot_results(pages)
        else:
            raise "Please choose a valid parser: tabula or camelot (not functional at the moment)"
    except Exception as e:
        logger.warning(f"\tThere are no tables in:\t{extract_pdf_file(pdf_link)}")
        logger.warning(f"\tException: {e}")
        return None
    return df


def downloaf_df_xlsx():
    pass
    # elif file_extension == "xlsx":
    #     df = get_planilha(encode_url(pdf_link))
    # logger.info("DONE!")
    # return df


def adjust_columns(df: pd.DataFrame, target_cols: list[str]):
    assert len(target_cols) == len(df.columns)
    df.columns = target_cols
    return df


def adjust_values_fields_old(df: pd.DataFrame, values_cols: list[str]):
    # TODO: Unit test
    for v in values_cols:
        df[v] = df[v].astype(str)
        df[v] = (
            df[v]
            .str.replace("[A-Za-z]", "", regex=True)
            .str.replace("$", "", regex=False)
            .str.replace("-", "", regex=False)
            .str.replace(" ", "", regex=False)
            .str.replace(")", "", regex=False)
            .str.replace("(", "", regex=False)
            .str.replace(".", "", regex=False)
            .str.replace(",", ".", regex=False)
        )
        df[v] = df[v].replace("", 0).astype(float)
    return df


def unpack_empenho_dash_dot(df: pd.DataFrame):
    if df.size > 0:
        df["empenho"] = df["empenho"].fillna("0.0-0")
        # ano.empenho-subempenho
        _df_split_subempenho = (
            df["empenho"].str.split("-", n=1, expand=True).fillna(value=0)
        )
        df["subempenho"] = _df_split_subempenho[1].astype(int)
        # ano.empenho
        _df_split_ano_empenho = (
            _df_split_subempenho[0].str.split(".", n=1, expand=True).fillna(value=0)
        )
        df["ano_empenho"], df["empenho"] = (
            _df_split_ano_empenho[0].astype(int),
            _df_split_ano_empenho[1].astype(int),
        )
    return df


def unpack_empenho_ne(df: pd.DataFrame) -> (int, int, int):
    if df.size > 0:
        df["empenho"] = df["empenho"].fillna("0NE0")
        _df_split = df["empenho"].str.split("NE", n=1, expand=True).fillna(value=0)
        _df_split[2] = 0
        df["ano_empenho"], df["empenho"], df["subempenho"] = (
            _df_split[0].astype(int),
            _df_split[1].astype(int),
            _df_split[2].astype(int),
        )
    return df


# def unpack_empenho(empenho_patterns: dict):
#     _dash_dot = empenho_patterns["dash_dot"]
#     _ne = empenho_patterns["ne"]
#
#     if _dash_dot is not None:
#         unpack_empenho_dash_dot(_dash_dot["df"], _dash_dot["null_pattern"])
#     if _ne is not None:
#         unpack_empenho_ne(_ne["df"], _ne["null_pattern"])
#
#
#     try:
#         elif null_pattern = "Both"
#     except:
#         logger.error(empenho)
#         logger.error("---")
#         raise
#
#     try:
#         _ano, _empenho, _subempenho = (
#             _ano.astype(int),
#             _empenho.astype(int),
#             _subempenho.astype(int),
#         )
#     except ValueError as e:
#         # for emp in empenho:
#         #     logger.warn(emp)
#         logger.warn(_ano.unique())
#         logger.warn(_empenho.unique())
#         logger.warn(_subempenho.unique())
#         raise e
#     except TypeError as e:
#         for emp in empenho:
#             logger.warn(emp)
#         logger.warn(_ano)
#         logger.warn(_empenho.unique())
#         logger.warn(_subempenho.unique())
#         raise e
#
#     return _ano, _empenho, _subempenho
#
