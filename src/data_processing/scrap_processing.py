#!/usr/bin/env python3
from data_processing.config import Settings
from pandas import DataFrame


def scrap_cleaning(df: DataFrame, config: Settings):
    "Cleaning scrap source data."

    # Remove special char in month
    df["month"] = df["month"].str.replace("\xa0", "")
    # Filter years
    if config.target_initial_year is not None:
        df = df[df["year"] >= config.target_initial_year]
    if config.target_last_year is not None:
        df = df[df["year"] <= config.target_last_year]
    # Filter links based on file extension
    df = df[df["pdf_link"].str.contains(f".{config.source_extension}", regex=False)]
    # Filter orgs
    df = df[df["org"].str.contains(config.target_org, regex=False)]
    # Generate full url for tables
    df["pdf_link"] = [
        url if url[:8] == "https://" else config.source_tables_url_base + url
        for url in df["pdf_link"]
    ]

    df = df.reset_index(drop=True)
    return df
