#!/usr/bin/env python3

import pandas as pd
from loguru import logger


def capitalize_str(df):
    cols = retrieve_str_cols(df)
    logger.info(f"Capitalize cols: {cols}")
    for col in cols:
        logger.info(f"Capitalize col: {col}")
        df[col] = df[col].str.upper()
    return df


def retrieve_str_cols(df):
    dt = df.dtypes
    str_cols = dt[dt == "object"].index
    return str_cols


def adjust_values_fields(df: pd.DataFrame, values_cols: list[str]):
    # TODO: Unit test
    for v in values_cols:
        df[v] = df[v].astype(str)

        # `empenho` pattern
        e_re = "^(?:\d*)(?:\.)(?:\d*)(?:-)(?:\d*)$"
        _e_mask = df[v].str.contains(e_re, na=False)
        logger.warning(df[v][_e_mask])
        if df[_e_mask].size > 0:
            if v == "valor_total":
                raise ValueError("Total values can't be null")
            logger.warning(
                f"\t\tColumn `{v}` has the pattern `empenho`: {df[v][_e_mask].head()}"
            )
            logger.warning("\t\tTransforming `XXX.XXX-XX` into `0`")
            df.loc[_e_mask, "empenho"] = df[v][_e_mask]
            df.loc[_e_mask, v] = "0"

        df[v] = (
            df[v]
            .str.replace("[A-Za-z]", "", regex=True)
            .str.replace("$", "", regex=False)
            .str.replace("-", "", regex=False)
            .str.replace(" ", "", regex=False)
            .str.replace(")", "", regex=False)
            .str.replace("(", "", regex=False)
            # .str.replace(".", "", regex=False)
            # .str.replace(",", ".", regex=False)
        )
        # df[v] = df[v].fillna("0")
        # df[v] = df[v].str.replace("\s", "0", regex=True)
        df[v] = df[v].replace("", "0")
        # 237,500,00
        # v_re = "^(\d*\.?)*(,?\d?\d?)$"

        # Pattern just comma
        # Commas as thousands and fraction separator
        jc_re = "^\d+(?:,\d{3})+(?P<comma>\,)(?P<faction>\d{2})$"
        _jc_mask = df[v].str.contains(jc_re, na=False)
        if df[_jc_mask].size > 0:
            logger.warning(
                f"\t\tColumn `{v}` has the pattern `just commas`: {df[v][_jc_mask].head()}"
            )
            logger.warning("\t\tTransforming `XXX.XXX.XX` into `XXX.XXX,XX`")
            df.loc[_jc_mask, v] = (
                df[v][_jc_mask]
                .apply(lambda s: str(s)[:-3] + "." + str(s)[-2:])
                .str.replace(",", "", regex=False)
                .str.replace(".", ",", regex=False)
            )

        v_re = "^\d+(.\d{3})*(\,\d{1,2})?$"
        _v_mask = df[v].str.contains(v_re, na=False)
        _df_neither = df[~_v_mask]
        if _df_neither.size > 0:
            # logger.warning(df[v][df[v].isna()])
            # logger.error(_df_neither[v].isna().any())
            logger.error(_df_neither)
            _df_neither.to_csv("neither.csv", index=False, sep=";")
            raise AssertionError(f"Values in `{v}` column can't be parsed")

        df[v] = (
            df[v].str.replace(".", "", regex=False).str.replace(",", ".", regex=False)
        )
        df[v] = df[v].astype(float)
    return df


def split_empenho_patterns(df: pd.DataFrame) -> dict:
    ne_re = "^(?:\d*)(?:NE)(?:\d*)$"
    dd_re = "^(?:\d*)(?:\.)(?:\d*)(?:-)(?:\d*)$"

    df["empenho"].fillna("0", inplace=True)

    # TODO: Parse as 0.0-0 pattern
    jn_re = "^(?:\d*)$"
    _jn_mask = df["empenho"].str.contains(jn_re, na=False)
    if df[_jn_mask].size > 0:
        logger.warning(
            f"\t\tColumn `empenho` has rows with the pattern `just number`:{df[_jn_mask].head()}"
        )
        logger.warning("\t\tTransforming `XXXX` into `XXXX.0-0`")
        df.loc[_jn_mask, "empenho"] = df["empenho"][_jn_mask].apply(
            lambda s: str(s) + ".0-0"
        )

    # TODO: How to parse: "SUB 2023.00207-10\rAG 2023.00210-1"
    # TODO: !!! Taking SUB slice, not AG !!!
    su_ag_re = "^(?:SUB )(?:\d*)(?:\.)(?:\d*)(?:-)(?:\d*)(?:.*)"
    _su_ag_mask = df["empenho"].str.contains(su_ag_re, na=False)
    if df[_su_ag_mask].size > 0:
        logger.warning(
            f"\t\tColumn `empenho` has rows with the pattern `SU_AG`:{df[_su_ag_mask].head()}"
        )
        logger.warning(
            "\t\tTransforming `SUB XXXX.XXX-XX\rAG YYYY.YYY-YY` into `YYYY.YYY-YY`"
        )
        su_ag_re_extract = "^(?:SUB )(?P<pattern>.*)(?:\r)"
        df.loc[_su_ag_mask, "empenho"] = df["empenho"][_su_ag_mask].str.extract(
            su_ag_re_extract
        )["pattern"]

    # TODO: Parse as 0.0-0 pattern
    d_d_re = "^(?:\d*)(?:\-)(?:\d*)(?:-)(?:\d*)$"
    _dash_dash_mask = df["empenho"].str.contains(d_d_re, na=False)
    if df[_dash_dash_mask].size > 0:
        logger.warning(
            f"\t\tColumn `empenho` has rows with the pattern `dash dash`:{df[_dash_dash_mask].head()}"
        )
        dash_dash_re_extract = "^(?P<year>\d*)(?P<dash>-)(?P<rest>.*)$"
        _re_lambda = lambda r: r.group("year") + "." + r.group("rest")
        df.loc[_dash_dash_mask, "empenho"] = df["empenho"][_dash_dash_mask].str.replace(
            dash_dash_re_extract, _re_lambda, regex=True
        )

    # TODO: NaN values?
    _df_nan = df[df["empenho"].isna()]

    # TODO: Despite the fact that NE is the new pattern, Dash Dot is the more general pattern
    # We will mantain both, but probabily we will need to remove `subempenho` and
    # transforme Dash Dot in NE, returning just one DataFrame.

    _df_ne = df[df["empenho"].str.contains(ne_re, na=False)]
    _df_dash_dot = df[df["empenho"].str.contains(dd_re, na=False)]

    # TODO: Use ?bisect? function
    _df_neither = df[
        ~df["empenho"].str.contains(dd_re, na=False)
        & ~df["empenho"].str.contains(ne_re, na=False)
        # & ~df["empenho"].str.contains(jn_re, na=False)
        # & ~df["empenho"].str.contains(su_ag_re, na=False)
        # & ~df["empenho"].str.contains(d_d_re, na=False)
        & ~df["empenho"].isna()
    ]

    if _df_neither.size > 0:
        logger.error(f"{_df_neither['empenho']}")
        raise "No patterns found"

    return {"dash_dot": _df_dash_dot, "ne": _df_ne}
