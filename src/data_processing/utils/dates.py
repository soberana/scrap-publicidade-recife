#!/usr/bin/env python3

def extract_month_from_anchor(anchor):
    """
    Extrai o mês da âncoira do link.

    Na página [0] a âncora do link contendo o pdf está no formato:
    [MÊS] de [ANO]
    Esta fuunção retira a primeira palavra e verifica se está é um mês válido

    [0] http://transparencia.recife.pe.gosv.br/codigos/web/estaticos/estaticos.php?nat=DDG&filhoNatureza=1249#filho
    """
    months = [
        "jan",
        "fev",
        "mar",
        "abr",
        "mai",
        "jun",
        "jul",
        "ago",
        "set",
        "out",
        "nov",
        "dez",
    ]

    month_str = anchor.split(" ")[0]
    month = month_str[:3].lower()
    if month in months:
        return month
    else:
        raise f"Can't extract month in {anchor}"
        return None
