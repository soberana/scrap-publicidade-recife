#!/usr/bin/env python3
import os
from pathlib import Path
from loguru import logger
from datetime import datetime

INITIAL_TIMESTAMP = datetime.now().strftime("%Y%m%d_%H%M%S")

def generate_data_dir(base_path: Path, directory: str):
    logger.info("Generating storage data path")

    directory = directory.replace(" ", "_").replace(".pdf", "")
    output_dir = base_path / f"{INITIAL_TIMESTAMP}/{directory}"

    if not os.path.exists(output_dir):
        logger.info(f"Creating {output_dir}")
        os.makedirs(output_dir, exist_ok=True)
    else:
        logger.info(f"Path exists: {output_dir}")
    return output_dir

def extract_pdf_file(pdf_link: str):
    return pdf_link.split("/")[-1]
