#!/usr/bin/env ipython
import pytest
from scrapy.exceptions import DropItem

from publicidade_recife.pipelines import extract_month


def test_extract_month_valid_months():
    assert extract_month("Janeiro") == "jan"
    assert extract_month("janeiro") == "jan"


def test_extract_month_invalid_months():
    with pytest.raises(DropItem) as exc_info:
        extract_month("Xaneiro")
