#!/usr/bin/env python3
import pandas as pd
import pytest

@pytest.fixture()
def df_test_01():
    df = pd.read_csv("tests/fixtures/df_test.csv", sep=';')
    df.info()
    return df
